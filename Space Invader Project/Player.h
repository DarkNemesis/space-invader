#pragma once
#include "Command.h"
#include "Entity.h"
#include "Weapon.h"
#include <HGE\hgefont.h>

class Player : public Entity
{	
	Weapon		mPrimaryWeapon;
	Weapon		mSecondaryWeapon;
	float		mSecondaryWeaponCharge;
	Weapon		mSpecialWeapon;

	Vector2f	mTopLeftBoundary;
	Vector2f	mBottomRightBoundary;

	float		mFuel;

	hgeAnimation*	mExplosionAnimation;
	HTEXTURE		mExplosionTexture;
	bool			mIsExploding;

	hgeFont*	mFont;
	float		mHUDX, mHUDY, mHUDW, mHUDH;
public:
	void	handleRealtimeInput(CommandQueue& commands, float dt);

	hgeRect	getCollisionRect();

	virtual void updateCurrent(float dt);
	virtual void drawCurrent();

	void	increaseHealth(float health);
	void	increaseAmmo(float ammo);
	void	increaseFuel(float fuel);

	void	explode();
	bool	isMarkedForRemoval();
	void	adaptPosition();

	Player();
	~Player();
};

