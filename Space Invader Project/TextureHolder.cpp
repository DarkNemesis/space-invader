#include "TextureHolder.h"
#include "SceneNode.h"

TextureHolder::TextureHolder()
{
}


TextureHolder::~TextureHolder()
{
}

void TextureHolder::load(Textures::Textures ID, string filename)
{
	unique_ptr<HTEXTURE> resource(new HTEXTURE(SceneNode::mHGE->Texture_Load(filename.c_str())));
	mResourceMap.insert(std::make_pair(ID, std::move(resource)));
}
HTEXTURE& TextureHolder::get(Textures::Textures ID)
{
	auto found = mResourceMap.find(ID);
	return *found->second;
}
