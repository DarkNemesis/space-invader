#pragma once
#include "Entity.h"
#include <HGE\hgeanim.h>

class Bullet : public Entity
{
	friend class Weapon;
public:
	void	updateCurrent(float dt);
	Bullet(HTEXTURE texture);
	~Bullet();
};

class Missile : public Bullet
{
	hgeAnimation*	mExplosionAnimation;
	bool			isExploding;
	HTEXTURE		mExplosionTexture;
	Vector2f		mTargetDestination;
public:
	Missile(HTEXTURE texture);

	hgeRect	getCollisionRect();
	void	setTargetDestination(Vector2f target);
	void	updateCurrent(float dt);
	void	drawCurrent();
	void	explode();
	void	destroy();
};

