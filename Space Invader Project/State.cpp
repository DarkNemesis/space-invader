#include "State.h"
#include "StateStack.h"

State::Context::Context(HGE& window, int& level)
: mHGEWindow(&window), mDifficultyLevel(level)
{
}

State::State(StateStack& stack, Context context) : mContext(context), mStack(&stack)
{
}

State::~State(void)
{
}

void State::requestStackPush(States::States stateID)
{
	mStack -> pushState(stateID);
}

void State::requestStackPop()
{
	mStack -> popState();
}

void State::requestStackCLear()
{
	mStack -> clearState();
}

State::Context State::getContext() const
{
	return mContext;
}