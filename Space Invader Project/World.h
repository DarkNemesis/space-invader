#pragma once
#include <set>
#include <array>
#include <HGE\hgefont.h>
#include "Player.h"
#include "Alien.h"
#include "SceneNode.h"
#include "TextureHolder.h"
#include "PowerUp.h"

enum Layer { BackGround, ForeGround, LayerCount};
enum UnitHierarchy { Units, Objects, UnitCount };

class World
{	
	struct ObjectPointers
	{
		SpriteNode* mSpriteNode;
		Player*		mPlayer;
	}	mEntities;

	int		mLevel;
		
	SceneNode mSceneGraph;
	array<SceneNode*, LayerCount> mSceneLayers;	
	array<SceneNode*, UnitCount> mSceneUnits;	

	array<float, 3> mSpawnTimers;

	hgeFont*	mFont;
	TextureHolder	mTextures;

	unsigned long mScore;
	unsigned long mKills;

	PowerClass	mPowerClass;

	default_random_engine mGenerator;

public:
	World(HGE* hge, int level);
	~World();
	CommandQueue&	getCommandQueue();
	void	update(float dt);
	void	loadTexture();
	void	buildScene();
	void	draw();
	void	handleCollision();
	bool	isGameRunning();
	void	spawnEnemies(float dt);
};

