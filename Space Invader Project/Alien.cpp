#include "Alien.h"
#define LEVEL_PARAM(a, b, c) (mLevel == 1) ? a : ((mLevel == 2) ? b : c)

Alien::Alien() : mPrimaryWeapon(this, NormalBullet)
{
	mBountyValue = 0;
	mCategory = Category::Enemy;	
	isExploding = false;
	mTarget = nullptr;
}

void Alien::setTarget(Entity* target)
{
	mTarget = target;
}

int Alien::getBountyValue()
{
	return mBountyValue;
}

int Alien::getRewardChance()
{
	return mRewardChance;
}
void Alien::explode()
{
	if (!isExploding)
	{
		mExplosionTexture = mTextureHolder->get(Textures::Explosion_Alien);
		mExplosionAnimation = new hgeAnimation(mExplosionTexture, 25, 30, 0, 0, 64, 64);
		mExplosionAnimation->SetHotSpot(32, 32);
		mExplosionAnimation->SetMode(HGEANIM_FWD | HGEANIM_NOLOOP);
		mExplosionAnimation->Play();
		isExploding = true;
		mCategory = Category::None;
	}
}

Alien::~Alien()
{
}

void Alien::updateCurrent(float dt)
{
	mPrimaryWeapon.update(dt);

	if (isExploding)
		mExplosionAnimation->Update(dt);
	if (isExploding && !mExplosionAnimation->IsPlaying())
		destroy();
	if (mHealth <= 0)
		explode();
	if (!isExploding)
		Entity::updateCurrent(dt);
	
}

void Alien::drawCurrent()
{
	if (isExploding)
		mExplosionAnimation->RenderEx(mPosition.x, mPosition.y, mRotation, mScale.x, mScale.y);
	else
		Entity::drawCurrent();
}

AlienF01::AlienF01()
{
	mRewardChance = 15;
	mBountyValue = 25;
	mTexture = mTextureHolder->get(Textures::Alien1);

	mSprite = new hgeSprite(mTexture, 0, 0, mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));
	mSprite->SetHotSpot(mHGE->Texture_GetWidth(mTexture) / 2, mHGE->Texture_GetWidth(mTexture) / 2);
	mSize = Vector2f(mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));

	mHealth = 2;

	random_device seed;
	mt19937 mGenerator(seed());	
	uniform_int_distribution<int> mUniformDistribution(1, 9);

	float x = mUniformDistribution(mGenerator) * mHGE->System_GetState(HGE_SCREENWIDTH) / 10;
	float y = -mSize.y * mScale.y / 2;
	mPosition = Vector2f(x, y);
	mVelocity = Vector2f(0, 100);
	mAcceleration = Vector2f(0, 0);
}

AlienF01::~AlienF01()
{
}

AlienF02::~AlienF02()
{
}

AlienF02::AlienF02()
{
	mRewardChance = LEVEL_PARAM(100, 75, 75);
	mBountyValue = 150;
	mTexture = mTextureHolder->get(Textures::Alien2);

	mSprite = new hgeSprite(mTexture, 0, 0, mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));
	mSprite->SetHotSpot(mHGE->Texture_GetWidth(mTexture) / 2, mHGE->Texture_GetWidth(mTexture) / 2);
	mSize = Vector2f(mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));
	mScale = Vector2f(mScaleFactor.x, mScaleFactor.y);

	mPrimaryWeapon.setPosition(Vector2f(0, mHGE->Texture_GetHeight(mTexture) * mScale.y / 2));
	mPrimaryWeapon.setFireSpeed(LEVEL_PARAM(4, 3, 2));
	mPrimaryWeapon.setCategory(Category::EnemyBullet);
	mPrimaryWeapon.setDamage(1);
	mPrimaryWeapon.setMissileSpeed(LEVEL_PARAM(125, 150, 175));
	mPrimaryWeapon.setTexture(Textures::Bullet_Floater);

	mHealth = LEVEL_PARAM(15, 20, 20);

	random_device seed;
	mt19937 mGenerator(seed());
	binomial_distribution<int> mBinomialDistribution(9, 0.5);
	uniform_int_distribution<int> mUniformDistribution(1, 9);

	float x = mUniformDistribution(mGenerator) * mHGE->System_GetState(HGE_SCREENWIDTH) / 10;
	float y = -mSize.y * mScale.y / 2;
	mPosition = Vector2f(x, y);
	mVelocity = Vector2f(0, 50);
	mAcceleration = Vector2f(0, 0);
}

void AlienF02::updateCurrent(float dt)
{
	if (mTarget != nullptr)
	{
		int y2 = mTarget->getPosition().y;
		int x2 = mTarget->getPosition().x;

		int y1 = mPosition.y;
		int x1 = mPosition.x;
		
		mRotation = atan2(x1 - x2, y2 - y1);
	}

	if (!isExploding)
	{
		unique_ptr<Entity> bullet = mPrimaryWeapon.fire(mTarget->getPosition());
		if (bullet)
			mObjectNode->attachChild(std::move(bullet));
	}

	Alien::updateCurrent(dt);
}

AlienF03::~AlienF03()
{
}

AlienF03::AlienF03()
{
	mRewardChance = 40;
	mBountyValue = 75;
	mIsOscillating = false;
	mTexture = mTextureHolder->get(Textures::Alien3);

	mSprite = new hgeSprite(mTexture, 0, 0, mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));
	mSprite->SetHotSpot(mHGE->Texture_GetWidth(mTexture) / 2, mHGE->Texture_GetWidth(mTexture) / 2);
	mSize = Vector2f(mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));
	mScale = Vector2f(mScaleFactor.x, mScaleFactor.y);

	mPrimaryWeapon.setPosition(Vector2f(0, mHGE->Texture_GetHeight(mTexture) * mScale.y / 2));
	mPrimaryWeapon.setFireSpeed(LEVEL_PARAM(7, 5, 5));
	mPrimaryWeapon.setCategory(Category::EnemyBullet);
	mPrimaryWeapon.setDamage(5);
	mPrimaryWeapon.setMissileSpeed(LEVEL_PARAM(500, 600, 650));
	mPrimaryWeapon.setTexture(Textures::Bullet_Sniper);

	mHealth = LEVEL_PARAM(3, 5, 5);

	random_device seed;
	mt19937 mGenerator(seed());
	binomial_distribution<int> mBinomialDistribution(9, 0.5);
	uniform_int_distribution<int> mUniformDistribution(1, 9);

	float x = mUniformDistribution(mGenerator) * mHGE->System_GetState(HGE_SCREENWIDTH) / 10;
	float y = -mSize.y * mScale.y / 2;
	mPosition = Vector2f(x, y);
	mVelocity = Vector2f(0, 75);
	mAcceleration = Vector2f(0, 0);

	binomial_distribution<int> frequency(20, 0.25);
	mFrequency = (frequency(mGenerator) + 20) / 100.f;

	binomial_distribution<int> amplitude(50, 0.75);
	mAmplitude = (amplitude(mGenerator) + 10);
	mOscillationSpeed = (amplitude(mGenerator) + 20);
	mStepMovement = 1;

	uniform_int_distribution<int> cp(mHGE->System_GetState(HGE_SCREENHEIGHT) * 0.2, mHGE->System_GetState(HGE_SCREENHEIGHT) * 0.8);
	mCenterPoint = cp(mGenerator);
}

void AlienF03::updateCurrent(float dt)
{
	if (mTarget != nullptr)
	{
		int y2 = mTarget->getPosition().y;
		int x2 = mTarget->getPosition().x;

		int y1 = mPosition.y;
		int x1 = mPosition.x;

		mRotation = atan2(x1 - x2, y2 - y1);
	}

	if (!isExploding)
	{
		unique_ptr<Entity> bullet = mPrimaryWeapon.fire(mTarget->getPosition());
		if (bullet)
			mObjectNode->attachChild(std::move(bullet));
	}

	if (mPosition.y > mCenterPoint)
	{
		mIsOscillating = true;
		setVelocity(Vector2f(0, 0));
	}

	if (mIsOscillating)
	{
		mPosition.x += mOscillationSpeed * dt * mStepMovement;
		mPosition.y = mCenterPoint + mAmplitude * sin(mPosition.x * mFrequency);

		if (mPosition.x > mHGE->System_GetState(HGE_SCREENWIDTH) * 0.9)
			mStepMovement = -1;

		if (mPosition.x < mHGE->System_GetState(HGE_SCREENWIDTH) * 0.1)
			mStepMovement = 1;
	}

	Alien::updateCurrent(dt);
}