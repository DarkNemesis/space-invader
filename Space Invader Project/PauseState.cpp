#include "PauseState.h"


PauseState::PauseState(StateStack& stack, Context context) : State(stack, context)
{
	mFont = new hgeFont("Fonts/font1.fnt");

	float width = getContext().mHGEWindow->System_GetState(HGE_SCREENWIDTH);
	float height = getContext().mHGEWindow->System_GetState(HGE_SCREENHEIGHT);

	mQuad.v[0].x = 0;		mQuad.v[0].y = 0;
	mQuad.v[1].x = width;	mQuad.v[1].y = 0;
	mQuad.v[2].x = width;	mQuad.v[2].y = height;
	mQuad.v[3].x = 0;		mQuad.v[3].y = height;

	for (int i = 0; i < 4; i++)
		mQuad.v[i].col = 0xFF2A3329;
}


PauseState::~PauseState()
{
}

bool PauseState::update(float dt)
{
	if (getContext().mHGEWindow->Input_KeyDown(HGEK_ESCAPE))
		requestStackPop();

	if (getContext().mHGEWindow->Input_KeyDown(HGEK_ENTER))
	{
		requestStackCLear();
		requestStackPush(States::Menu);
	}

	return false;
}

void PauseState::draw()
{
	float width = getContext().mHGEWindow->System_GetState(HGE_SCREENWIDTH);
	float height = getContext().mHGEWindow->System_GetState(HGE_SCREENHEIGHT);

	getContext().mHGEWindow->Gfx_RenderQuad(&mQuad);
	mFont->printfb(width * 0.40, height * 0.45, width * 0.20, height * 0.10, HGETEXT_CENTER, "Press ESC To Resume\nPress ENTER To Quit");
}