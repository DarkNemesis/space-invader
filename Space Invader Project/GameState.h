#pragma once
#include "World.h"
#include "Player.h"
#include "State.h"
class GameState : public State
{
	//The game world where everything happens
	World mWorld;
public:
	GameState(StateStack& stack, Context context);
	~GameState(void);
	virtual bool update(float dt);
	virtual void draw();
};

