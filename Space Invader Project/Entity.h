#pragma once
#include "SceneNode.h"
#include "TextureHolder.h"

//Entity is an object that has a velocity, acceleration, can move and interact with the environment.
//Aliens, Bullets etc. all are an entity in the game.
class Entity : public SceneNode
{	
protected:	
	Vector2f	mVelocity;
	Vector2f	mAcceleration;
	
	Vector2f	mSize;
	hgeSprite*	mSprite;

	int			mHealth;
	bool		mDestroyed;
	
	SceneNode*	mObjectNode;
	HTEXTURE	mTexture;

public:
	Entity();

	void	setObjectNode(SceneNode* node);

	void	setHealth(int health);
	int		getHealth();

	void		setVelocity(Vector2f velocity);
	Vector2f	getVelocity();

	void		setAcceleration(Vector2f acceleration);
	Vector2f	getAcceleration();

	void	damage(int dmg);
	bool	isDestroyed();
	bool	isMarkedForRemoval();
	hgeRect	getCollisionRect();
	
	virtual void	updateCurrent(float dt);
	virtual void	drawCurrent();		
	virtual void	destroy();	
};

