#pragma once

namespace Category
{
	enum Category
	{
		None = 0,
		Scene = 1 << 0,
		Player = 1 << 1,
		Enemy = 1 << 2,
		Power = 1 << 3,
		EnemyBullet = 1 << 4,
		PlayerBullet = 1 << 5,
		PowerUp = 1 << 6
	};
}

namespace States
{
	enum States
	{
		None,
		Title,
		Menu,
		Load,
		Game,
		Pause
	};
}

namespace Textures
{
	enum Textures
	{
		Player,
		Alien1,
		Alien2,
		Alien3,
		Background_Gameplay,
		Bullet_Normal,
		Bullet_Plasma,
		Bullet_PlasmaCharged,
		Bullet_Floater,
		Bullet_Missile,
		Bullet_Sniper,
		Power_Health,
		Power_Fuel,
		Power_Ammo,
		Explosion_Alien,
		Explosion_Player,
		Explosion_Missile,
		TotalTextureCount
	};
}

namespace MusicTheme
{
	enum MusicTheme
	{
		MenuTheme,
		GameTheme
	};
}

namespace SFX
{
	enum SFX
	{
		
	};
}
