#include "SceneNode.h"

HGE* SceneNode::mHGE;
CommandQueue SceneNode::mCommandQueue;
Vector2f SceneNode::mScaleFactor;
int	SceneNode::mLevel;
TextureHolder* SceneNode::mTextureHolder;

void SceneNode::attachChild(SN_PTR child)
{
	child -> mParent = this;
	mChildren.push_back(std::move(child));
}

unique_ptr<SceneNode> SceneNode::detachChild(SceneNode& node)
{
	auto found = std::find_if(mChildren.begin(), mChildren.end(), [&] (SN_PTR& p) {return p.get() == &node;});
	SN_PTR result = std::move(*found);
	result -> mParent = nullptr;
	mChildren.erase(found);
	return result;
}

void SceneNode::deleteAllChild()
{
	for (SN_PTR& child : mChildren)
	{
		child->deleteAllChild();
		SN_PTR result = std::move(child);
	}
	mChildren.clear();
}

void SceneNode::updateCurrent(float dt)
{}

void SceneNode::update(float dt)
{
	updateCurrent(dt);
	updateChildren(dt);
}

void SceneNode::updateChildren(float dt)
{
	for(SN_PTR& child : mChildren)
		child->update(dt);
}

void SceneNode::draw()
{
	drawCurrent();
	drawChildren();
}

void SceneNode::drawCurrent()
{}

void SceneNode::drawChildren()
{
	for(const SN_PTR& child : mChildren)
		child -> draw();
}



unsigned int SceneNode::getCategory() const
{
	return mCategory;
}

unsigned int SceneNode::getCollidingCategory() const
{
	return mCollidingCategory;
}

void SceneNode::setCategory(unsigned int category)
{
	mCategory = category;
}

void SceneNode::setCollidingCategory(unsigned int category)
{
	mCollidingCategory = category;
}

void SceneNode::checkDoesNodeCollide(vector<SceneNode*>& collidingNodes)
{
	if (mDoesItCollide)
		collidingNodes.push_back(this);
	for (unique_ptr<SceneNode>& child : mChildren)
		child->checkDoesNodeCollide(collidingNodes);
}
void SceneNode::checkSceneCollision(SceneNode& sceneGraph, set<Pair>& collisionPairs)
{
	checkNodeCollision(sceneGraph, collisionPairs);
	for (unique_ptr<SceneNode>& child : sceneGraph.mChildren)
		checkSceneCollision(*child, collisionPairs);
}

hgeRect SceneNode::getCollisionRect() 
{
	return hgeRect(0, 0, 0, 0);
}

bool SceneNode::checkCollision(SceneNode& lhs, SceneNode& rhs)
{
	hgeRect rRect = rhs.getCollisionRect();
	if (lhs.getCollisionRect().Intersect(&rRect))	
		return true;
	return false;
}

void SceneNode::checkNodeCollision(SceneNode& node, set<Pair>& collisionPairs)
{
	if (this != &node && checkCollision(*this, node))
		collisionPairs.insert(minmax(this, &node));
	for (unique_ptr<SceneNode>& child : mChildren)
		child->checkNodeCollision(node, collisionPairs);
}

bool SceneNode::isMarkedForRemoval()
{
	return false;
}

bool SceneNode::isDestroyed()
{
	return false;
}

void SceneNode::removeNodes()
{
	auto wreckfield = remove_if(mChildren.begin(), mChildren.end(), mem_fn(&SceneNode::isMarkedForRemoval));
	mChildren.erase(wreckfield, mChildren.end());

	std::for_each(mChildren.begin(), mChildren.end(), mem_fn(&SceneNode::removeNodes));
}

void SceneNode::onCommand(Command& command, float dt)
{
	if (command.mCategory & getCategory())
		command.mAction(*this, dt);

	for (SN_PTR& child : mChildren)
		child->onCommand(command, dt);
}

SpriteNode::SpriteNode() : mDefaultVelocity(100), mThresholdVelocity(400)
{		
	mIsShaking = false;
	mCategory = Category::Scene;
	mVelocity = mDefaultVelocity;
	mAcceleration = 0;
	mDistance = 0;

	//Load the texture
	mTexture = mTextureHolder->get(Textures::Background_Gameplay);
	
	//Initialize the sprites
	for (int i = 0; i < 2; i++)
	{
		mSprites[i] = new hgeSprite(mTexture, 0, 0, mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));
		mSprites[i]->SetHotSpot(0, 0);		
	}	

	//Scale the sprites according to window size
	int width = mHGE->System_GetState(HGE_SCREENWIDTH);
	int height = mHGE->System_GetState(HGE_SCREENHEIGHT);	
	setScale(width * 1.0 / mSprites[0]->GetWidth(), height * 1.0 / mSprites[0]->GetHeight());

	//Set the position of sprites
	for (int i = 0; i < 2; i++)
	{
		mPositions[i] = Vector2f(0, (i - 1) * mHGE->Texture_GetHeight(mTexture) * mScale.y);
	}
}

void SpriteNode::updateCurrent(float dt)
{
	for (int i = 0; i < 2; i++)
	{
		mPositions[i].y += mVelocity * dt * mScaleFactor.y;
		if (mPositions[i].y >= mHGE->Texture_GetHeight(mTexture) * mScale.y)
		{
			mPositions[i].y = -mHGE->Texture_GetHeight(mTexture) * mScale.y;
			mPositions[(i + 1) % 2].y = 0 * mScale.y;
			break;
		}			
	}
	mDistance += mVelocity * dt * mScaleFactor.y;

	mVelocity += mAcceleration * dt;

	if (mVelocity > mThresholdVelocity)
		mVelocity = mThresholdVelocity;
	
	if (mVelocity > mDefaultVelocity)
		mAcceleration = -150;
	else
		mAcceleration = 0;

	if (mVelocity < mDefaultVelocity)
		mVelocity = mDefaultVelocity;

	if (mIsShaking)
	{
		int inc = (mTremorCounter > 0) ? 1 : -1;
		for (int i = 0; i < 2; i++)
			mPositions[i].x += inc * 10;

		if (abs(mPositions[0].x) >= abs(mTremorCounter) * 10)
		{
			mTremorCounter = (abs(mTremorCounter) - 1) * -inc;

			for (int i = 0; i < 2; i++)
				mPositions[i].x -= inc * 10;
		}
			

		if (mTremorCounter == 0)
		{
			mIsShaking = false;
			for (int i = 0; i < 2; i++)
				mPositions[i].x = 0;
		}
	}
}

void SpriteNode::accelerate()
{
	mAcceleration = 100;
}

void SpriteNode::deaccelerate()
{
	mAcceleration = -50;
}

int SpriteNode::getDistanceTravelled()
{
	return mDistance;
}

void SpriteNode::shake()
{
	if (!mIsShaking)
	{
		mIsShaking = true;
		mTremorCounter = 7;
	}
}

void SpriteNode::drawCurrent()
{
	for (int i = 0; i < 2; i++)
	{
		mSprites[i]->RenderEx(mPositions[i].x, mPositions[i].y, 0, mScale.x, mScale.y);
	}
}






