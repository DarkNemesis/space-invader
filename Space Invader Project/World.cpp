#include "World.h"
#include <fstream>

//Helps to Tweak Paramters based on difficulty
#define LEVEL_PARAM(a, b, c) (mLevel == 1) ? a : ((mLevel == 2) ? b : c)

//Helps to Tweak Parameters based on distance travelled
#define DIST_PARAM(b, a) max(a, b - mEntities.mSpriteNode->getDistanceTravelled() / 5000)

//Used for matching categories of two nodes that have collided. 
//The nodes are rearranged and placed in the same order as the one passed in the args.
bool matchCategory(SceneNode::Pair& pair, Category::Category type1, Category::Category type2)
{
	unsigned int category1 = pair.first->getCategory();
	unsigned int category2 = pair.second->getCategory();
	if (type1 & category1 && type2 & category2)
		return true;
	else if (type1 & category2 && type2 & category1)
	{
		swap(pair.first, pair.second);
		return true;
	}
	else return false;
}

//Frees all the textures
World::~World()
{
	for (int i = 0; i < Textures::TotalTextureCount; i++)
	{
		auto t = static_cast<Textures::Textures>(i);
		SceneNode::mHGE->Texture_Free(mTextures.get(t));
	}		
}

World::World(HGE* hge, int level) : mSceneGraph(), mSceneLayers()
{
	mScore = 0;
	mKills = 0;
	mLevel = level;

	SceneNode::mHGE = hge;
	SceneNode::mLevel = mLevel;

	loadTexture();
}

//Loads all the textures here
void World::loadTexture()
{
	mTextures.load(Textures::Background_Gameplay, "Images/Background_Gameplay.jpg");

	mTextures.load(Textures::Player, "Images/ship.png");

	mTextures.load(Textures::Bullet_Normal,			"Images/Bullet_Normal.png");
	mTextures.load(Textures::Bullet_Plasma,			"Images/Bullet_Plasma.png");
	mTextures.load(Textures::Bullet_PlasmaCharged,	"Images/Bullet_PlasmaCharged.png");
	mTextures.load(Textures::Bullet_Floater,		"Images/Bullet_Floater.png");
	mTextures.load(Textures::Bullet_Missile,		"Images/Bullet_Missile.png");
	mTextures.load(Textures::Bullet_Sniper,			"Images/Bullet_Sniper.png");

	mTextures.load(Textures::Alien1,	"Images/AlienShip_1.png");
	mTextures.load(Textures::Alien2,	"Images/AlienShip_2.png");
	mTextures.load(Textures::Alien3,	"Images/AlienShip_3.png");

	mTextures.load(Textures::Power_Health,	"Images/Power_Health.png");
	mTextures.load(Textures::Power_Fuel,	"Images/Power_Fuel.png");
	mTextures.load(Textures::Power_Ammo,	"Images/Power_Ammo.png");

	mTextures.load(Textures::Explosion_Alien,	"Images/Explosion_Alien.png");
	mTextures.load(Textures::Explosion_Missile,	"Images/Explosion_Missile.png"); 
	mTextures.load(Textures::Explosion_Player,	"Images/Explosion_Player.png");

	mFont = new hgeFont("Fonts/font1.fnt");

	SceneNode::mTextureHolder = &mTextures;
	SceneNode::mScaleFactor = Vector2f(SceneNode::mHGE->System_GetState(HGE_SCREENWIDTH) / 1920.f, SceneNode::mHGE->System_GetState(HGE_SCREENHEIGHT) / 1080.f);
	buildScene();
}

//Builds the main world. ALl the layers in the game are created.
void World::buildScene()
{
	//Create the different layers in the World
	for (int i = 0; i < LayerCount; i++)
	{
		unique_ptr<SceneNode> layer(new SceneNode());
		mSceneLayers[i] = layer.get();
		mSceneGraph.attachChild(move(layer));
	}

	/******************************
	Intitalize the background layer
	******************************/

	//The background
	unique_ptr<SpriteNode> backgroundSprite(new SpriteNode());
	mEntities.mSpriteNode = backgroundSprite.get();
	mSceneLayers[BackGround]->attachChild(move(backgroundSprite));

	/******************************
	Intitalize the foreground layer
	******************************/

	//Create the different node layers for different entities
	for (int i = 0; i < UnitCount; i++)
	{
		unique_ptr<SceneNode> layer(new SceneNode());
		mSceneUnits[i] = layer.get();
		mSceneLayers[ForeGround]->attachChild(move(layer));
	}

	/*********************************
	Load the Level and Build the Scene
	*********************************/

	//Spawn the Player
	unique_ptr<Player> player(new Player());
	player->setObjectNode(mSceneUnits[Objects]);
	mEntities.mPlayer = player.get();
	mSceneUnits[Units]->attachChild(move(player));

	mPowerClass.setObjectNode(mSceneUnits[Objects]);
}

//Updates the world
void World::update(float dt)
{
	//Processes all the communication in between nodes.
	while (!SceneNode::mCommandQueue.isEmpty())
		mSceneGraph.onCommand(SceneNode::mCommandQueue.pop(), dt);

	//Processes all the real time input from the user
	mEntities.mPlayer->handleRealtimeInput(SceneNode::mCommandQueue, dt);

	//Handles collisions between the nodes
	handleCollision();

	//Remove nodes that are no longer needed
	mSceneGraph.removeNodes();

	//Spawn Enemies in the Game World
	spawnEnemies(dt);

	//Updates each node
	mSceneLayers[BackGround]->update(dt);
	mSceneLayers[ForeGround]->update(dt);
}

bool World::isGameRunning()
{
	return !mEntities.mPlayer->isDestroyed();
}

CommandQueue& World::getCommandQueue()
{
	return SceneNode::mCommandQueue;
}

//Spawns enemies based on timer and random values.
//Random Values uses binomial_distribution and the args are tweaked based on difficulty and distance travelled.
void World::spawnEnemies(float dt)
{
	if (mSpawnTimers[0] <= 0)
	{
		unique_ptr<AlienF01> alien(new AlienF01());
		alien->setObjectNode(mSceneUnits[Objects]);		

		int a = DIST_PARAM((LEVEL_PARAM(6, 4, 4)), 2);
		float b = LEVEL_PARAM(0.75, 0.5, 0.30);
		binomial_distribution<int> timer(a, b);
		mSpawnTimers[0] = timer(mGenerator);

		a = LEVEL_PARAM(5, 7, 9);
		b = LEVEL_PARAM(0.30, 0.5, 0.75);
		binomial_distribution<int> speed(a, b);
		alien->setVelocity(Vector2f(0, alien->getVelocity().y * speed(mGenerator)));
		mSceneUnits[Units]->attachChild(move(alien));
	}
	else
		mSpawnTimers[0] -= dt;

	if (mSpawnTimers[1] <= 0)
	{
		unique_ptr<AlienF02> alien(new AlienF02());
		alien->setObjectNode(mSceneUnits[Objects]);
		alien->setTarget(mEntities.mPlayer);;
		mSceneUnits[Units]->attachChild(move(alien));

		int a = DIST_PARAM((LEVEL_PARAM(10, 8, 8)), 5);
		float b = LEVEL_PARAM(1, 0.7, 0.5);
		binomial_distribution<int> timer(a, b);
		mSpawnTimers[1] = timer(mGenerator);
	}
	else
		mSpawnTimers[1] -= dt;

	if (mSpawnTimers[2] <= 0)
	{
		unique_ptr<AlienF03> alien(new AlienF03());
		alien->setObjectNode(mSceneUnits[Objects]);
		alien->setTarget(mEntities.mPlayer);;
		mSceneUnits[Units]->attachChild(move(alien));

		int a = DIST_PARAM((LEVEL_PARAM(8, 8, 7)), 4);
		float b = LEVEL_PARAM(1, 0.7, 0.5);
		binomial_distribution<int> timer(a, b);
		mSpawnTimers[2] = timer(mGenerator);
	}
	else
		mSpawnTimers[2] -= dt;
}

//Draws every node and the HUD contents
void World::draw()
{
	mSceneLayers[BackGround]->draw();
	mSceneLayers[ForeGround]->draw();

	mFont->printf(5, 5, HGETEXT_LEFT, "Distance: %0.2fm\nLevel: %d", mEntities.mSpriteNode->getDistanceTravelled() / 1000.f, mLevel);
	mFont->printf(SceneNode::mHGE->System_GetState(HGE_SCREENWIDTH) * 0.90, 5, HGETEXT_RIGHT, "Score: %d\nKills: %d", mScore, mKills);
}

//Handles collision between two nodes
void World::handleCollision()
{
	set<SceneNode::Pair> collisionPairs;
	mSceneLayers[ForeGround]->checkSceneCollision(*mSceneLayers[ForeGround], collisionPairs);

	for (auto pair : collisionPairs)
	{
		if (matchCategory(pair, Category::Player, Category::Enemy))
		{
			Player& player = static_cast<Player&>(*pair.first);
			Alien& alien = static_cast<Alien&>(*pair.second);
			alien.explode();
			player.damage(alien.getHealth());
		}
		else if (matchCategory(pair, Category::PlayerBullet, Category::Enemy))
		{
			Bullet& bullet = static_cast<Bullet&>(*pair.first);
			Alien& alien = static_cast<Alien&>(*pair.second);

			int a = alien.getHealth();
			alien.damage(bullet.getHealth());
			int b = max(0, alien.getHealth());

			int dmg = a - b;
			int score = min((dmg * (dmg + 1) / 2), 75);
			mScore += score * 5;
			bullet.destroy();

			if (alien.getHealth() <= 0)
			{
				mKills += 1;
				mScore += alien.getBountyValue();
				mPowerClass.powerRoll(alien.getRewardChance(), alien.getPosition());
			}
		}
		else if (matchCategory(pair, Category::Player, Category::EnemyBullet))
		{
			Player& player = static_cast<Player&>(*pair.first);
			Bullet& bullet = static_cast<Bullet&>(*pair.second);
			player.damage(bullet.getHealth());
			bullet.destroy();
		}
		else if (matchCategory(pair, Category::Player, Category::PowerUp))
		{
			Player& player = static_cast<Player&>(*pair.first);
			PowerUp& powerUp = static_cast<PowerUp&>(*pair.second);
			powerUp.executePower(player);
			powerUp.destroy();
		}
		else if (matchCategory(pair, Category::PlayerBullet, Category::EnemyBullet))
		{
			Bullet& Bullet_Normal = static_cast<Bullet&>(*pair.first);
			Bullet& Bullet_Plasma = static_cast<Bullet&>(*pair.second);
			int a = Bullet_Normal.getHealth();
			int b = Bullet_Plasma.getHealth();
			Bullet_Normal.damage(b);
			Bullet_Plasma.damage(a);
		}
	}
}
