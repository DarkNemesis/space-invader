#pragma once
#include "StateStack.h"
#include "Identifiers.h"
#include "Player.h"
#include "GameState.h"
#include "MenuState.h"
#include "PauseState.h"
#include <HGE/hge.h>

//The Class that manages the HGE Interface and the different states in a game
class Application
{
	//HGE Window
	static HGE*			mWindow;

	//Stack that holds the various application states
	static StateStack*	mStateStack;

	static int			mDifficultyLevel;
public:
	Application(void);
	~Application(void);

	static bool update();
	static bool render();
	static void init();

	static void SetWindowed(bool windowed);
	
	static void registerStates();
};

