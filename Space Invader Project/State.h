#pragma once
#include "Identifiers.h"
#include <HGE\hge.h>
#include <memory>

using namespace std;
class StateStack;

class State
{
public: 
	struct Context
	{
		Context(HGE& window, int& level);
		HGE*		mHGEWindow;
		int&		mDifficultyLevel;
	};

	State(StateStack& stack, Context context);
	virtual ~State(void);
	virtual void draw() = 0;
	virtual bool update(float dt) = 0;
		
protected:
	void requestStackPush(States::States stateID);
	void requestStackPop();
	void requestStackCLear();
	Context getContext() const;

private:
	Context mContext;
	StateStack* mStack;
};

