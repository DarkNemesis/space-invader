#pragma once
#include <map>
#include <memory>
#include <string>
#include <HGE\hge.h>
#include "Identifiers.h"
using namespace std;

//Maps all the textures with an enum Identifier
class TextureHolder
{
	map<Textures::Textures, unique_ptr<HTEXTURE>> mResourceMap;
public:
	void load(Textures::Textures ID, string filename);
	HTEXTURE& get(Textures::Textures ID);
public:
	TextureHolder();
	~TextureHolder();
};

