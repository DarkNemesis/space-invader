#pragma once
#include <functional>
#include <queue>
#include "Identifiers.h"

using namespace std;
class SceneNode;
class Command
{
public:
	Command(void);
	~Command(void);
	function<void (SceneNode&, float dt)> mAction;
	unsigned int mCategory;
};

class CommandQueue
{
	queue<Command> mQueue;
public :
	void push(Command& command);
	Command pop();
	bool isEmpty();
};
