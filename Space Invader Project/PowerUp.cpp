#include "PowerUp.h"
#define LEVEL_PARAM(a, b, c) (SceneNode::mLevel == 1) ? a : ((SceneNode::mLevel == 2) ? b : c)

PowerUp::PowerUp(HTEXTURE texture)
{
	mTexture = texture;

	mSprite = new hgeSprite(mTexture, 0, 0, mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));
	mSprite->SetHotSpot(mHGE->Texture_GetWidth(mTexture) / 2, mHGE->Texture_GetWidth(mTexture) / 2);
	mSize = Vector2f(mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));
}


PowerUp::~PowerUp()
{
}

PowerClass::PowerClass()
{
	mExecutionFunctions[Power_Health] = [](Player& player)
	{
		player.increaseHealth(5);
	};

	mExecutionFunctions[Power_Fuel] = [](Player& player)
	{
		player.increaseFuel(25);
	};

	mExecutionFunctions[AmmoPower] = [](Player& player)
	{
		player.increaseAmmo(2);
	};

	mTextures[Power_Health] = Textures::Power_Health;
	mTextures[Power_Fuel] = Textures::Power_Fuel;
	mTextures[AmmoPower] = Textures::Power_Ammo;

	mTimer = 0;
}

void PowerClass::powerRoll(int chance, Vector2f position)
{
	random_device seed;
	mt19937 mGenerator(seed());
	uniform_int_distribution<int> roll(1, 100);

	if (roll(mGenerator) <= chance)
	{
		int powerUp = roll(mGenerator);
		if (powerUp <= 50)
			createPowerUp(Power_Fuel, position);
		else if (powerUp <= 75)
			createPowerUp(Power_Health, position);
		else
			createPowerUp(AmmoPower, position);
	}
}

void PowerClass::setObjectNode(SceneNode* node)
{
	mObjectNode = node;
}

void PowerClass::createPowerUp(int i, Vector2f position)
{
	unique_ptr<PowerUp> powerUp(new PowerUp(Entity::mTextureHolder->get(mTextures[i])));
	powerUp->setVelocity(Vector2f(0, LEVEL_PARAM(250, 300, 350)));

	random_device seed;
	mt19937 mGenerator(seed());
	uniform_int_distribution<int> mUniformDistribution(2, 8);

	powerUp->setPosition(position);
	powerUp->setCategory(Category::PowerUp);
	powerUp->executePower = mExecutionFunctions[i];
	mObjectNode->attachChild(move(powerUp));
}
