#include "Weapon.h"


Weapon::Weapon(Entity* source, BulletTypes type) : mSource(source)
{
	mBulletType = type;
	mChargeLevels = 1;
	mUnitChargeDuration = 0;
	mAmmunitionCount = -1;
}

void Weapon::setPosition(Vector2f position)
{
	mPosition = position;
}

void Weapon::setDamage(float damage)
{
	mDamage = damage;
}

void Weapon::setFireSpeed(float firespeed)
{
	mFireSpeed = firespeed;
}

void Weapon::setMissileSpeed(float missileSpeed)
{
	mMissileSpeed = missileSpeed;
}

void Weapon::update(float dt)
{
	if (mCoolDown > 0)
		mCoolDown -= dt;
	if (mCoolDown < 0)
		mCoolDown = 0;
}

void Weapon::setCategory(unsigned int category)
{
	mCategory = category;
}

void Weapon::setChargeDetail(int level, float unitDuration)
{
	mChargeLevels = level;
	mUnitChargeDuration = unitDuration;
}

void Weapon::setTexture(Textures::Textures texture)
{
	mTexture = texture;
}

void Weapon::setChargedTexture(Textures::Textures texture)
{
	mChargedTexture = texture;
}

int Weapon::getCharge(float charge)
{
	return min((int)(charge / mUnitChargeDuration), mChargeLevels);
}

unique_ptr<Entity> Weapon::fire(Vector2f target, float charge)
{
	if (mAmmunitionCount == 0)
		return nullptr;

	if (mCoolDown == 0)
	{
		if (mUnitChargeDuration > 0 && charge < mUnitChargeDuration)
			return nullptr;

		int chargeLevel = 1;
		if (mUnitChargeDuration > 0)
			chargeLevel = min((int)(charge / mUnitChargeDuration), mChargeLevels);

		mCoolDown = mFireSpeed;

		Textures::Textures tex = mTexture;
		if (mChargeLevels > 1 && chargeLevel > ceil(mChargeLevels / 2))
			tex = mChargedTexture;

		float sine = sin(mSource->getRotation());
		float cosine = cos(mSource->getRotation());
		float rx = mPosition.x * cosine - mPosition.y * sine;
		float ry = mPosition.x * sine + mPosition.y * cosine;

		unique_ptr<Bullet> bullet;
		if (mBulletType == BulletTypes::MissileBullet)
		{
			bullet.reset(new Missile(Entity::mTextureHolder->get(tex)));
			static_cast<Missile*>(bullet.get())->setTargetDestination(target);
		}
		else
			bullet.reset(new Bullet(Entity::mTextureHolder->get(tex)));

		bullet->setPosition(mSource->getPosition().x + rx, mSource->getPosition().y + ry);
		bullet->setCategory(mCategory);
		bullet->setHealth(mDamage * chargeLevel);
		
		float x1 = target.x;
		float y1 = target.y;
		float x2 = bullet->getPosition().x;
		float y2 = bullet->getPosition().y;
		int sx = (x2 > x1) ? -1 : 1;
		int sy = (y2 > y1) ? -1 : 1;

		float m, x = 0, y = mMissileSpeed;

		if (x1 != x2)			
		{
			m = tan(atan2((y2 - y1),(x2 - x1)));
			x = mMissileSpeed / sqrt(1 + pow(m, 2));
			y = abs(m) * x;
			
			bullet->setRotation(atan2(x1 - x2, y2 - y1));
		}

		bullet->setVelocity(Vector2f(x * sx, y * sy));

		if (mAmmunitionCount != -1)
			mAmmunitionCount--;

		return(move(bullet));
	}
	else
		return nullptr;
}

int Weapon::getAmmunitionCount()
{
	return mAmmunitionCount;
}
void Weapon::increaseAmmunition(int ammo)
{
	if (mAmmunitionCount != -1)
		mAmmunitionCount += ammo;
}
void Weapon::setAmmunition(int ammo)
{
	mAmmunitionCount = ammo;
}

Weapon::~Weapon()
{
}
