#pragma once
struct Vector2f
{
	float x;
	float y;

	Vector2f()
	{
		x = 0;
		y = 0;
	}
	Vector2f(float x1, float y1)
	{
		x = x1;
		y = y1;
	}
};

class Drawable
{	
protected:
	Vector2f mPosition;
	Vector2f mScale;
	float mRotation;
public:
	Drawable();
	~Drawable();

	void setPosition(Vector2f position);
	void setPosition(float x, float y);

	void setRotation(float rotation);

	void setScale(Vector2f scale);
	void setScale(float x, float y);

	float		getRotation();
	Vector2f	getPosition();	
	Vector2f	getScale();

	void move(Vector2f displacement);
	void move(float x, float y);
};

