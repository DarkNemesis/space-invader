#include "Player.h"

Player::Player() : mPrimaryWeapon(this, NormalBullet), mSecondaryWeapon(this, NormalBullet), mSpecialWeapon(this, MissileBullet)
{
	mIsExploding = false;

	mFuel = 100;
	mHealth = 20;
	
	mCategory = Category::Player;
	mTexture = mTextureHolder->get(Textures::Player);

	mSprite = new hgeSprite(mTexture, 0, 0, mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));
	mSprite->SetHotSpot(mHGE->Texture_GetWidth(mTexture) / 2, mHGE->Texture_GetWidth(mTexture) / 2);

	int width = mHGE->System_GetState(HGE_SCREENWIDTH);
	mTopLeftBoundary.x = 0.10 * width;
	mBottomRightBoundary.x = 0.90 * width;

	int height = mHGE->System_GetState(HGE_SCREENHEIGHT);
	mTopLeftBoundary.y = 0.20 * height;
	mBottomRightBoundary.y = 0.95 * height;

	setPosition(width / 2, mBottomRightBoundary.y - mSprite->GetHeight() / 2);

	mHUDX = 0.025 * width;
	mHUDW = 0.50 * width;
	mHUDY = 0.825 * height;
	mHUDH = 0.10 * height;
	mFont = new hgeFont("Fonts/font1.fnt");

	mPrimaryWeapon.setPosition(Vector2f(0, -mHGE->Texture_GetHeight(mTexture) / 2));
	mPrimaryWeapon.setFireSpeed(0.2);
	mPrimaryWeapon.setCategory(Category::PlayerBullet);
	mPrimaryWeapon.setDamage(1);
	mPrimaryWeapon.setMissileSpeed(500);
	mPrimaryWeapon.setTexture(Textures::Bullet_Normal);

	mSecondaryWeaponCharge = 0;
	mSecondaryWeapon.setPosition(Vector2f(0, -mHGE->Texture_GetHeight(mTexture) / 2));
	mSecondaryWeapon.setFireSpeed(0);
	mSecondaryWeapon.setCategory(Category::PlayerBullet);
	mSecondaryWeapon.setDamage(3);
	mSecondaryWeapon.setMissileSpeed(300);
	mSecondaryWeapon.setChargeDetail(4, 0.5);
	mSecondaryWeapon.setTexture(Textures::Bullet_Plasma);
	mSecondaryWeapon.setChargedTexture(Textures::Bullet_PlasmaCharged);

	mSpecialWeapon.setPosition(Vector2f(0, -mHGE->Texture_GetHeight(mTexture) / 2));
	mSpecialWeapon.setFireSpeed(3);
	mSpecialWeapon.setCategory(Category::PlayerBullet);
	mSpecialWeapon.setDamage(500);
	mSpecialWeapon.setMissileSpeed(100);
	mSpecialWeapon.setAmmunition(5);
	mSpecialWeapon.setChargeDetail(0, 0);
	mSpecialWeapon.setTexture(Textures::Bullet_Missile);
}

Player::~Player()
{
}

void Player::updateCurrent(float dt)
{	
	if (mIsExploding)
	{
		mExplosionAnimation->Update(dt);
		if (!mExplosionAnimation->IsPlaying())
			mDestroyed = true;

		return;
	}
		
	mVelocity.x += mAcceleration.x * dt;
	mVelocity.y += mAcceleration.y * dt;

	if (mAcceleration.y == 0)
		mFuel -= 0.25 * dt;

	else if (abs(mAcceleration.y) == 500)
		mFuel -= 1 * dt;

	else if (abs(mAcceleration.x) == 200 || abs(mAcceleration.y) == 700)
		mFuel -= 2.5 * dt;
	
	move(mVelocity.x * dt, mVelocity.y * dt);

	adaptPosition();

	if (mHealth <= 0 || mFuel <= 0)
		explode();

	mPrimaryWeapon.update(dt);
	mSecondaryWeapon.update(dt);
	mSpecialWeapon.update(dt);
}

void Player::explode()
{
	if (!mIsExploding)
	{
		mExplosionTexture = mTextureHolder->get(Textures::Explosion_Player);
		mExplosionAnimation = new hgeAnimation(mExplosionTexture, 24, 12, 0, 0, 128, 128);
		mExplosionAnimation->SetHotSpot(64, 64);
		mExplosionAnimation->SetMode(HGEANIM_FWD | HGEANIM_NOLOOP);
		mExplosionAnimation->Play();
		mIsExploding = true;
		mCategory = Category::None;
	}
}

bool Player::isMarkedForRemoval()
{
	return false;
}

void Player::drawCurrent()
{
	if (mIsExploding)
		mExplosionAnimation->RenderEx(mPosition.x, mPosition.y, mRotation, mScale.x, mScale.y);
	else
		mSprite->RenderEx(mPosition.x, mPosition.y, 0, mScale.x, mScale.y);

	mFont->printfb(mHUDX, mHUDY, mHUDW, mHUDH, HGETEXT_LEFT, "Charge: %d\nHealth: %d\nFuel: %d\nMissiles: %d", 
		mSecondaryWeapon.getCharge(mSecondaryWeaponCharge), max(0, mHealth * 5), (int)ceil(mFuel), mSpecialWeapon.getAmmunitionCount());
}

hgeRect	Player::getCollisionRect()
{
	hgeRect alpha;
	return *(mSprite->GetBoundingBoxEx(mPosition.x, mPosition.y, mRotation, mScale.x, mScale.y, &alpha));
}

void Player::increaseHealth(float health)
{
	mHealth += health;

	if (mHealth > 20)
		mHealth = 20;
}
void Player::increaseAmmo(float ammo)
{
	mSpecialWeapon.increaseAmmunition(ammo);
}
void Player::increaseFuel(float fuel)
{
	mFuel += fuel;

	if (mFuel > 150)
		mFuel = 150;
}

void Player::adaptPosition()
{
	if (mPosition.x - (mSprite->GetWidth() * mScale.x) / 2 < mTopLeftBoundary.x)
		setPosition(mTopLeftBoundary.x + (mSprite->GetWidth() * mScale.x) / 2, mPosition.y);

	if (mPosition.x + (mSprite->GetWidth() * mScale.x) / 2 > mBottomRightBoundary.x)
		setPosition(mBottomRightBoundary.x - (mSprite->GetWidth() * mScale.x) / 2, mPosition.y);

	if (mPosition.y - (mSprite->GetHeight() * mScale.y) / 2 < mTopLeftBoundary.y)
		setPosition(mPosition.x, mTopLeftBoundary.y + (mSprite->GetHeight() * mScale.y) / 2);

	if (mPosition.y + (mSprite->GetHeight() * mScale.y) / 2 > mBottomRightBoundary.y)
		setPosition(mPosition.x, mBottomRightBoundary.y - (mSprite->GetHeight() * mScale.y) / 2);
}

void Player::handleRealtimeInput(CommandQueue& commands, float dt)
{
	int boost = 0;
	if (mHGE->Input_GetKeyState(HGEK_SHIFT))
		boost = 200;

	if (mHGE->Input_GetKeyState(HGEK_LEFT))
	{
		mVelocity.x = -500;
		mAcceleration.x = -boost;
	}

	if (mHGE->Input_GetKeyState(HGEK_RIGHT))
	{
		mVelocity.x = 500;
		mAcceleration.x = boost;
	}

	if (mHGE->Input_GetKeyState(HGEK_UP))
	{		
		mAcceleration.y = -500 - boost;
	}

	if (mHGE->Input_GetKeyState(HGEK_DOWN))
	{
		mAcceleration.y = 500 + boost;		
	}

	if (mHGE->Input_KeyUp(HGEK_UP) || mHGE->Input_KeyUp(HGEK_DOWN))
	{
		mAcceleration.y = 0;
		mVelocity.y = 0;
	}

	if (mHGE->Input_KeyUp(HGEK_LEFT) || mHGE->Input_KeyUp(HGEK_RIGHT))
	{
		mAcceleration.x = 0;
		mVelocity.x = 0;
	}

	if (mHGE->Input_GetKeyState(HGEK_Q))
	{
		mSecondaryWeaponCharge = 0;
		unique_ptr<Entity> bullet = mPrimaryWeapon.fire(Vector2f(mPosition.x, 0));
		if (bullet)
			mObjectNode->attachChild(std::move(bullet));
	}

	if (mHGE->Input_GetKeyState(HGEK_W))
	{
		mSecondaryWeaponCharge += dt;
	}

	if (mHGE->Input_KeyUp(HGEK_W))
	{
		unique_ptr<Entity> bullet = mSecondaryWeapon.fire(Vector2f(mPosition.x, 0), mSecondaryWeaponCharge);
		if (bullet)
			mObjectNode->attachChild(std::move(bullet));
		mSecondaryWeaponCharge = 0;
	}

	if (mHGE->Input_KeyDown(HGEK_E))
	{
		if (mFuel > 5)
		{
			unique_ptr<Entity> bullet = mSpecialWeapon.fire(Vector2f(mHGE->System_GetState(HGE_SCREENWIDTH) / 2, mHGE->System_GetState(HGE_SCREENHEIGHT) / 2));
			if (bullet)
			{
				mObjectNode->attachChild(std::move(bullet));
				mFuel -= 5;
			}
		}
	}
}
