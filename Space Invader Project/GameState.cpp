#include "GameState.h"

GameState::GameState(StateStack& stack, Context context) : mWorld(context.mHGEWindow, context.mDifficultyLevel), State(stack, context)
{
}


GameState::~GameState(void)
{
}


bool GameState::update(float dt)
{
	if (getContext().mHGEWindow->Input_KeyDown(HGEK_ESCAPE))
		requestStackPush(States::Pause);

	if (!mWorld.isGameRunning())
	{
		requestStackPop();
		requestStackPush(States::Menu);
	}		
	else
		mWorld.update(dt);

	return true;
}

void GameState::draw()
{
	mWorld.draw();	
}