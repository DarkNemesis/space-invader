#pragma once
#include "Entity.h"
#include "Weapon.h"
#include <random>
#include <chrono>
#include <HGE\hgeanim.h>
class Alien : public Entity
{
protected:
	hgeAnimation*	mExplosionAnimation;
	HTEXTURE		mExplosionTexture;
	bool			isExploding;
	Weapon			mPrimaryWeapon;
	Entity*			mTarget;
	int				mBountyValue;
	int				mRewardChance;
public:	
			Alien();
			~Alien();
	void	explode();
	int		getBountyValue();
	int		getRewardChance();
	void	setTarget(Entity* target);
	virtual void	updateCurrent(float dt);
	virtual void	drawCurrent();
};

class AlienF01 : public Alien
{
public:
	AlienF01();
	~AlienF01();
};

class AlienF02 : public Alien
{
public:
	void	updateCurrent(float dt);
	AlienF02();
	~AlienF02();
};

class AlienF03 : public Alien
{
	float	mFrequency;
	int		mAmplitude;
	int		mCenterPoint;
	int		mStepMovement;
	int		mOscillationSpeed;
	bool	mIsOscillating;
public:
	void	updateCurrent(float dt);
	AlienF03();
	~AlienF03();
};


