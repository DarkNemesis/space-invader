#pragma once
#include "Entity.h"
#include "Player.h"
#include <random>
#include <functional>
class PowerUp : public Entity
{
public:
	friend class PowerClass;
	function<void(Player&)> executePower;
	PowerUp(HTEXTURE texture);
	~PowerUp();
};

enum PowerType {Power_Health, Power_Fuel, AmmoPower, PowerCount};

class PowerClass
{
	array<function<void(Player&)>, PowerCount>	mExecutionFunctions;
	array<Textures::Textures, PowerCount>		mTextures;
	SceneNode*	mObjectNode;
	float	mTimer;

	void createPowerUp(int i, Vector2f position);
public:
	PowerClass();
	void setObjectNode(SceneNode* node);
	void powerRoll(int chance, Vector2f position);	
};