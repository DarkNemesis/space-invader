#pragma once
#include <HGE\hge.h>
#include <HGE\hgesprite.h>
#include <HGE\hgerect.h>

#include <memory>
#include <bitset>
#include <set>
#include <vector>
#include <array>
#include <iostream>
#include <algorithm>
#include <functional>

#include "Drawable.h"
#include "TextureHolder.h"
#include "Command.h"

using namespace std;

//SceneNode is a basic unit in this game. Everything in this game is primitively a scenenode.
class SceneNode : public Drawable
{
public :
	static	HGE*			mHGE;
	static	CommandQueue	mCommandQueue;
	static	Vector2f		mScaleFactor;
	static	int				mLevel;
	static	TextureHolder*	mTextureHolder;

	typedef unique_ptr<SceneNode>			SN_PTR;
	typedef pair<SceneNode*, SceneNode*>	Pair;

protected : 
	vector<unique_ptr<SceneNode>> mChildren;
	SceneNode* mParent;
	unsigned int mCategory;
	unsigned int mCollidingCategory;

	bool mDoesItCollide{ 0 };
public:	
	//Functions for packing children
	void					attachChild(unique_ptr<SceneNode> child);
	unique_ptr<SceneNode>	detachChild(SceneNode& node);
	void					deleteAllChild();
	
	//Updating functions
	void					update(float dt);
	virtual void			updateCurrent(float dt);
	void					updateChildren(float dt);

	//Rendering functions
	virtual void			draw();
	virtual void			drawCurrent();
	void					drawChildren();

	virtual unsigned int	getCategory() const;
	virtual void			setCategory(unsigned int category);
	virtual unsigned int	getCollidingCategory() const;
	virtual void			setCollidingCategory(unsigned int category);

	void					onCommand(Command& command, float dt);
	
	//Collision functions
	virtual hgeRect			getCollisionRect();
	bool					checkCollision(SceneNode& lhs, SceneNode& rhs);
	virtual void			checkNodeCollision(SceneNode& node, set<Pair>& collisionPairs);
	virtual void			checkSceneCollision(SceneNode& sceneGraph, set<Pair>& collisionPairs);
	virtual void			checkDoesNodeCollide(vector<SceneNode*>& collidingNodes);
	
	//Node removing functions
	virtual bool			isMarkedForRemoval();
	virtual bool			isDestroyed();
	void					removeNodes();
};


class SpriteNode : public SceneNode
{
	float mVelocity;
	float mAcceleration;

	const float mThresholdVelocity;
	const float mDefaultVelocity;

	array<hgeSprite*, 2> mSprites;
	array<Vector2f, 2> mPositions;
	HTEXTURE mTexture;

	double	mDistance;

	bool	mIsShaking;
	int		mTremorCounter;

public:	
	SpriteNode();
	void accelerate();
	void deaccelerate();
	int		getDistanceTravelled();
	void	shake();
	virtual void updateCurrent(float dt);
	virtual void drawCurrent();
};

