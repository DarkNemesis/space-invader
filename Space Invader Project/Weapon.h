#pragma once
#include "Entity.h"
#include "Bullet.h"

enum BulletTypes {NormalBullet, HomingBullet, MissileBullet};
enum AmmunitionType {Limited, Infinite};
enum EnergyType {Instant, Charged};

//Weapon class
//Simulates the artillery of a ship
//All ships have HAS-A relationship with a weapon class. Weapon, in itself, is neither an entity nor a node.
class Weapon
{
	float	mFireSpeed;
	float	mCoolDown;
	float	mDamage;
	float	mMissileSpeed;
	int		mAmmunitionCount;
	Entity*	mSource;
	Vector2f		mPosition;

	int		mChargeLevels;
	float	mUnitChargeDuration;
	
	unsigned int		mCategory;
	Textures::Textures	mTexture;
	Textures::Textures	mChargedTexture;
	BulletTypes		mBulletType;
public:
	Weapon(Entity* source, BulletTypes type);
	~Weapon();

	int		getAmmunitionCount();
	void	increaseAmmunition(int ammo);

	void	setAmmunition(int ammo);
	void	setCategory(unsigned int category);
	void	setPosition(Vector2f position);
	void	setFireSpeed(float firespeed);
	void	setDamage(float damage);
	void	setMissileSpeed(float missileSpeed);
	void	setChargeDetail(int level, float unitDuration);
	int		getCharge(float charge);
	void	setTexture(Textures::Textures texture);
	void	setChargedTexture(Textures::Textures texture);

	void	update(float dt);

	unique_ptr<Entity>	fire(Vector2f target, float charge = 0);	
};

