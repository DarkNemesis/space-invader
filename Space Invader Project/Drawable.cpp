#include "Drawable.h"
#include "SceneNode.h"

Drawable::Drawable()
{
	mRotation = 0;
	mScale = Vector2f(1.f, 1.f);
}


Drawable::~Drawable()
{
}

void Drawable::setPosition(Vector2f position)
{
	mPosition = position;
}
void Drawable::setPosition(float x, float y)
{
	mPosition = Vector2f(x, y);
}

void Drawable::setRotation(float rotation)
{
	mRotation = rotation;
}

void Drawable::setScale(Vector2f scale)
{
	mScale = scale;
}

void Drawable::setScale(float x, float y)
{
	mScale = Vector2f(x, y);
}

float Drawable::getRotation()
{
	return mRotation;
}
Vector2f Drawable::getPosition()
{
	return mPosition;
}
Vector2f Drawable::getScale()
{
	return mScale;
}

void Drawable::move(Vector2f displacement)
{
	mPosition.x += (displacement.x * SceneNode::mScaleFactor.x);
	mPosition.y += (displacement.y * SceneNode::mScaleFactor.y);
}

void Drawable::move(float x, float y)
{
	mPosition.x += (x * SceneNode::mScaleFactor.x);
	mPosition.y += (y * SceneNode::mScaleFactor.y);
}