/*
** Haaf's Game Engine 1.7
** Copyright (C) 2003-2007, Relish Games
** hge.relishgames.com
**
** Tutorial 06 - Creating menus
*/

// In menuitem.cpp/h we define the
// behaviour of our custom GUI control

#include <HGE\hge.h>
#include <HGE\hgegui.h>
#include <HGE\hgefont.h>
#include <HGE\hgecolor.h>
#include <functional>

using namespace std;
class MenuItem : public hgeGUIObject
{
public:
	MenuItem(int id, hgeFont *fnt, HEFFECT snd, float x, float y, float delay, char *title);

	virtual void	Render();
	virtual void	Update(float dt);

	virtual void	Enter();
	virtual void	Leave();
	virtual bool	IsDone();
	virtual void	Focus(bool bFocused);
	virtual void	MouseOver(bool bOver);

	virtual bool	MouseLButton(bool bDown);
	virtual bool	KeyClick(int key, int chr);

	void	setAction(function<void()> click);

private:
	function<void()>	mAction;

	hgeFont*	fnt;
	HEFFECT		snd;
	float		delay;
	char*		title;

	hgeColor	scolor, dcolor, scolor2, dcolor2, sshadow, dshadow;
	hgeColor	color, shadow;
	float		soffset, doffset, offset;
	float		timer, timer2;
};
