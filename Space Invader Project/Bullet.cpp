#include "Bullet.h"
#include "Alien.h"

Bullet::Bullet(HTEXTURE texture)
{
	mTexture = texture; 

	mSprite = new hgeSprite(mTexture, 0, 0, mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));
	mSprite->SetHotSpot(mHGE->Texture_GetWidth(mTexture) / 2, mHGE->Texture_GetWidth(mTexture) / 2);
	mSize = Vector2f(mHGE->Texture_GetWidth(mTexture), mHGE->Texture_GetHeight(mTexture));
}

void Bullet::updateCurrent(float dt)
{
	if (mPosition.y <= 0 || mHealth <= 0 || mPosition.y > mHGE->System_GetState(HGE_SCREENHEIGHT))
		destroy();
	else
		Entity::updateCurrent(dt);
}

Bullet::~Bullet()
{
}

void Missile::drawCurrent()
{
	if (isExploding)
		mExplosionAnimation->RenderEx(mPosition.x, mPosition.y, mRotation, mScale.x, mScale.y);
	else
		Entity::drawCurrent();
}

void Missile::explode()
{
	if (!isExploding)
	{
		mExplosionTexture = mTextureHolder->get(Textures::Explosion_Missile);
		mExplosionAnimation = new hgeAnimation(mExplosionTexture, 32, 32, 0, 0, 256, 256);
		setScale(4 * mScaleFactor.x, 4 * mScaleFactor.y);
		mExplosionAnimation->SetHotSpot(256/2, 256/2);
		mExplosionAnimation->SetMode(HGEANIM_FWD | HGEANIM_NOLOOP);
		mExplosionAnimation->Play();
		isExploding = true;

		Command cmd;
		cmd.mCategory = Category::Scene;
		cmd.mAction = [](SceneNode& node, float dt)
		{SpriteNode& sn = static_cast<SpriteNode&>(node); sn.shake(); };
		mCommandQueue.push(cmd);
	}
}

Missile::Missile(HTEXTURE texture) : Bullet(texture)
{
}

void Missile::destroy()
{
	explode();
}

hgeRect	Missile::getCollisionRect()
{
	hgeRect alpha;
	if (isExploding)
	{
		alpha.x1 = 0;
		alpha.y1 = 0;
		alpha.x2 = mHGE->System_GetState(HGE_SCREENWIDTH);
		alpha.y2 = mHGE->System_GetState(HGE_SCREENHEIGHT);
		return alpha;
	}
	else
		return Entity::getCollisionRect();		
}

void Missile::updateCurrent(float dt)
{
	if (isExploding)
	{
		if (!mExplosionAnimation->IsPlaying())
			mDestroyed = true;
		else
			mExplosionAnimation->Update(dt);
	}
		
	else if (abs(mTargetDestination.x - mPosition.x) <= 1 && abs(mTargetDestination.y - mPosition.y) <= 1)
		explode();
	else if (mPosition.y <= 0 || mHealth < 0 || mPosition.y > mHGE->System_GetState(HGE_SCREENHEIGHT))
		destroy();
	else
		Entity::updateCurrent(dt);
}

void Missile::setTargetDestination(Vector2f target)
{
	mTargetDestination = target;
}
