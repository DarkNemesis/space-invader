#include "MenuState.h"

MenuState::MenuState(StateStack& stack, Context context) : State(stack, context)
{
	mGoBackToPreviousMenu = [this](){mGUIStack.pop(); mGUIStack.top()->Enter(); };

	// Load sound and textures
	mBackgrondTexture = getContext().mHGEWindow->Texture_Load("Images/Background_Menu.jpg");
	mInstructionTexture = getContext().mHGEWindow->Texture_Load("Images/Information_Instruction.png");
	mAlienpediaTexture = getContext().mHGEWindow->Texture_Load("Images/Information_Alienpedia.png");

	tex = getContext().mHGEWindow->Texture_Load("Images/Cursor.png");	
	snd = getContext().mHGEWindow->Effect_Load("Sound/Menu.wav");
	
	float width = getContext().mHGEWindow->System_GetState(HGE_SCREENWIDTH);
	float height = getContext().mHGEWindow->System_GetState(HGE_SCREENHEIGHT);

	mBackgroundSprite = new hgeSprite(mBackgrondTexture, 0, 0, 1920, 1080);
	mBackgroundSprite->SetHotSpot(0, 0);
	
	// Load the font, create the cursor sprite
	fnt = new hgeFont("Fonts/font1.fnt");
	spr = new hgeSprite(tex, 0, 0, 32, 32);

	mFont = new hgeFont("Fonts/font1.fnt");
	mFont->SetColor(0xFFFF0000);
	mFont->SetScale(2);

	// Create and initialize the GUI
	hgeGUI* GUIMain = new hgeGUI();

	auto playButton = new MenuItem(1, fnt, snd, width / 2, height / 2, 0.0f, "Play");
	playButton->setAction([this, GUIMain]()
	{
		GUIMain->Leave();
		mPostGUILeaveAction = [this](){requestStackPop(); requestStackPush(States::Game); }; 
	});
	GUIMain->AddCtrl(playButton);

	auto difficultyButton = new MenuItem(2, fnt, snd, width / 2, height / 2 + 40, 0.1f, "Difficulty");
	difficultyButton->setAction([this, width, height, GUIMain]()
	{
		mGUIStack.top()->Leave();
		mPostGUILeaveAction = [this, width, height]()
		{
			hgeGUI* GUI = new hgeGUI(); 

			array<char*, 3> titles{ { "Easy" ,  "Medium" ,  "Hard" } };
			for (int i = 0; i < 3; i++)
			{		
				auto item = new MenuItem(i + 1, fnt, snd, width / 2, height / 2 + i * 40, 0.0f, titles[i]);
				item->setAction([this, GUI, i]()
				{
					GUI->Leave();
					getContext().mDifficultyLevel = i + 1;
					mPostGUILeaveAction = mGoBackToPreviousMenu;
				});
				GUI->AddCtrl(item);
			}

			GUI->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
			GUI->SetCursor(spr);
			GUI->SetFocus(2);
			mGUIStack.push(GUI);
			mGUIStack.top()->Enter();
		};
	});
	GUIMain->AddCtrl(difficultyButton);

	auto instructionButton = new MenuItem(3, fnt, snd, width / 2, height / 2 + 80, 0.2f, "Instructions");
	instructionButton->setAction([this, GUIMain, width, height]()
	{
		GUIMain->Leave();

		mPostGUILeaveAction = [this, width, height]()
		{
			hgeGUI* GUI = new hgeGUI();

			mBackgroundSprite->SetTexture(mInstructionTexture);
	
			GUI->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
			GUI->SetCursor(spr);
			GUI->Enter();
			mGUIStack.push(GUI);
		};
	});
	GUIMain->AddCtrl(instructionButton);
	
	auto alienpedia = new MenuItem(4, fnt, snd, width / 2, height / 2 + 120, 0.3f, "Alienpedia");
	alienpedia->setAction([this, GUIMain, width, height]()
	{
		GUIMain->Leave();

		mPostGUILeaveAction = [this, width, height]()
		{
			hgeGUI* GUI = new hgeGUI();

			mBackgroundSprite->SetTexture(mAlienpediaTexture);

			GUI->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
			GUI->SetCursor(spr);
			GUI->Enter();
			mGUIStack.push(GUI);
		};
	});
	GUIMain->AddCtrl(alienpedia);

	auto exitButton = new MenuItem(5, fnt, snd, width / 2, height / 2 + 160, 0.4f, "Exit");
	exitButton->setAction([this, GUIMain]() 
	{
		GUIMain->Leave();
		mPostGUILeaveAction = [this](){requestStackPop();};
	});
	GUIMain->AddCtrl(exitButton);

	GUIMain->SetNavMode(HGEGUI_UPDOWN | HGEGUI_CYCLED);
	GUIMain->SetCursor(spr);
	GUIMain->SetFocus(1);
	GUIMain->Enter();

	mGUIStack.push(GUIMain);
}


MenuState::~MenuState(void)
{}

bool MenuState::update(float dt)
{
	if (mGUIStack.top()->Update(dt) == -1)
	{
		mPostGUILeaveAction();
		mPostGUILeaveAction = [](){};
	}

	if (getContext().mHGEWindow->Input_GetKeyState(HGEK_ESCAPE) && mGUIStack.size() > 1)
	{
		mGUIStack.top()->Leave();
		mPostGUILeaveAction = mGoBackToPreviousMenu;
	}

	if (mGUIStack.size() == 1)
		mBackgroundSprite->SetTexture(mBackgrondTexture);
		
	return true;
}


void MenuState::draw()
{
	float width = getContext().mHGEWindow->System_GetState(HGE_SCREENWIDTH);
	float height = getContext().mHGEWindow->System_GetState(HGE_SCREENHEIGHT);

	mBackgroundSprite->RenderEx(0, 0, 0, width * 1.0 / mBackgroundSprite->GetWidth(), height * 1.0 / mBackgroundSprite->GetHeight());

	mGUIStack.top()->Render();	

	fnt->SetColor(0xFFFFFFFF);

	if (mGUIStack.size() == 1)
	{
		mFont->printfb(width * 0.40, height * 0.20, width * 0.20, height * 0.10, HGETEXT_CENTER, "Space Invader");
		fnt->printfb(width * 0.05, height * 0.90, width * 0.40, height * 0.10, HGETEXT_LEFT, "Created By: Ekshit Nalwaya");
	}	
}

