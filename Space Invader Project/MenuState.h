#pragma once
#include <deque>
#include <stack>
#include <array>
#include <vector>
#include <HGE\hge.h>
#include <HGE\hgegui.h>
#include <HGE\hgefont.h>
#include <HGE\hgecolor.h>
#include <HGE\hgeguictrls.h>

#include "State.h"
#include "fstream"
#include "MenuItem.h"

//This class is partly taken from HGE tutorials
//The additional thing in this class is ability to have sub menus, and I am using lambda functions instead of waiting for Update() to return an id.
class MenuState : public State
{
	// Some resource handles
	HEFFECT		snd;
	HTEXTURE	tex;

	HTEXTURE	mBackgrondTexture;
	HTEXTURE	mInstructionTexture;
	HTEXTURE	mAlienpediaTexture;
	hgeSprite*	mBackgroundSprite;

	hgeFont		*mFont;

	// Pointers to the HGE objects we will use
	hgeFont				*fnt;
	hgeSprite			*spr;

	stack<hgeGUI*>		mGUIStack;
	function<void()>	mPostGUILeaveAction;
	function<void()>	mGoBackToPreviousMenu;

public:
	MenuState(StateStack& stack, Context context);
	~MenuState(void);
	virtual void draw();
	virtual bool update(float dt);
};

