#include "Application.h"

HGE* Application::mWindow;
StateStack* Application::mStateStack;
int	Application::mDifficultyLevel = 2;

Application::Application(void)
{}
Application::~Application(void)
{}

//Intitialize the Application Window
void Application::init()
{	
	mWindow = hgeCreate(HGE_VERSION);

	mWindow->System_SetState(HGE_FRAMEFUNC, &Application::update);
	mWindow->System_SetState(HGE_RENDERFUNC, &Application::render);
	mWindow->System_SetState(HGE_TITLE, "Space Invader");
	mWindow->System_SetState(HGE_USESOUND, true);

	//Get Available Video Modes
	//HGE has issues, even at full screen, default resolution is 800x600 -_-
	//Found this solution on internet
	DEVMODE dm = { 0 };
	dm.dmSize = sizeof(dm);
	for (int iModeNum = 0; EnumDisplaySettings(NULL, iModeNum, &dm) != 0; iModeNum++);

	mWindow->System_SetState(HGE_WINDOWED, false);
	mWindow->System_SetState(HGE_SCREENWIDTH, dm.dmPelsWidth);
	mWindow->System_SetState(HGE_SCREENHEIGHT, dm.dmPelsHeight);
	mWindow->System_SetState(HGE_FPS, 120);

	mStateStack = new StateStack(State::Context(*mWindow, mDifficultyLevel));

	if (mWindow->System_Initiate())
	{
		registerStates();

		//Begin the application with first state as Menu
		mStateStack->pushState(States::States::Menu);

		mWindow->System_Start();
	}

	mWindow->System_Shutdown();
	mWindow->Release();
}

//Update the Application
bool Application::update()
{
	//Update the stack
	mStateStack->update(mWindow->Timer_GetDelta());

	//If all states have been popped, exit the application
	if (mStateStack->isEmpty())
		return true;

	return false;
}

//Draw the Application
bool Application::render()
{
	mWindow->Gfx_BeginScene();
	mWindow->Gfx_Clear(0);

	//Draw the Stack
	mStateStack->draw();

	mWindow->Gfx_EndScene();

	return false;
}

//Register the various states of the Application
void Application::registerStates()
{
	mStateStack->registerState<MenuState>(States::States::Menu);
	mStateStack->registerState<GameState>(States::States::Game);
	mStateStack->registerState<PauseState>(States::States::Pause);
}