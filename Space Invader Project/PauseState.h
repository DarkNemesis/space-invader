#pragma once
#include "State.h"
#include <HGE\hgefont.h>
#include <HGE\hgecolor.h>
class PauseState : public State
{
	hgeFont*	mFont;
	hgeQuad		mQuad;
public:
	PauseState(StateStack& stack, Context context);
	~PauseState();
	virtual bool update(float dt);
	virtual void draw();
};

