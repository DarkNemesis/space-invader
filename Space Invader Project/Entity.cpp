#include "Entity.h"

Entity::Entity()
{
	mDestroyed = false;
	mScale = mScaleFactor;
	mAcceleration = Vector2f(0, 0);
}

void Entity::setObjectNode(SceneNode* node)
{
	mObjectNode = node;
}

void Entity::setHealth(int health)
{
	mHealth = health;
}

int Entity::getHealth()
{
	return mHealth;
}

void Entity::damage(int dmg)
{
	mHealth -= dmg;
}

void Entity::updateCurrent(float dt)
{
	mVelocity.x += mAcceleration.x * dt;
	mVelocity.y += mAcceleration.y * dt;

	move(mVelocity.x * dt, mVelocity.y * dt);

	if (mPosition.y - (mSize.y * mScale.y / 2) > mHGE->System_GetState(HGE_SCREENHEIGHT))
		destroy();

	if (mPosition.y + (mSize.y * mScale.y / 2) < 0)
		destroy();
}

void Entity::setAcceleration(Vector2f acceleration)
{
	mAcceleration = acceleration;
}
Vector2f  Entity::getAcceleration()
{
	return mAcceleration;
}

void Entity::setVelocity(Vector2f velocity)
{
	mVelocity = velocity;
}

Vector2f Entity::getVelocity()
{
	return mVelocity;
}

hgeRect	Entity::getCollisionRect()
{
	hgeRect alpha;
	return *(mSprite->GetBoundingBoxEx(mPosition.x, mPosition.y, mRotation, mScale.x, mScale.y, &alpha));
}

void Entity::drawCurrent()
{
	mSprite->RenderEx(mPosition.x, mPosition.y, mRotation, mScale.x, mScale.y);
}

bool Entity::isMarkedForRemoval()
{
	return mDestroyed;
}
bool Entity::isDestroyed()
{
	return mDestroyed;
}
void Entity::destroy()
{
	mDestroyed = true;
}